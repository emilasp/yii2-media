<?php

namespace emilasp\media\models;

use emilasp\core\helpers\FileHelper;
use emilasp\core\helpers\UrlHelper;
use emilasp\users\common\models\User;
use ReflectionClass;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\variety\behaviors\VarietyModelBehavior;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * Class File
 * @package emilasp\media\models
 */
class File extends ActiveRecord
{
    /** Типы файлов */
    const TYPE_FILE_IMAGE   = 'image';
    const TYPE_FILE_XLS     = 'xls';
    const TYPE_FILE_DOC     = 'doc';
    const TYPE_FILE_PDF     = 'pdf';
    const TYPE_FILE_FILE    = 'file';
    const TYPE_FILE_ARCHIVE = 'archive';
    const TYPE_FILE_VIDEO   = 'video';
    const TYPE_FILE_AUDIO   = 'audio';

    /** Стандартные размеры */
    const SIZE_ICO = 'ico';
    const SIZE_MIN = 'min';
    const SIZE_MID = 'mid';
    const SIZE_MED = 'med';
    const SIZE_MAX = 'max';
    const SIZE_ORG = 'org';


    /** @var array Типы файла */
    public static $types = [
        self::TYPE_FILE_IMAGE   => 'image',
        self::TYPE_FILE_FILE    => 'file',
        self::TYPE_FILE_DOC     => 'word',
        self::TYPE_FILE_XLS     => 'excel',
        self::TYPE_FILE_PDF     => 'pdf',
        self::TYPE_FILE_ARCHIVE => 'archive',
        self::TYPE_FILE_VIDEO   => 'video',
        self::TYPE_FILE_AUDIO   => 'audio',
    ];


    /** @var string файл из формы */
    public $file;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media_file';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['object_id', 'status', 'created_by', 'updated_by', 'is_main'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'description', 'name'], 'string', 'max' => 255],
            [['params'], 'string'],
            [['type', 'attribute'], 'string', 'max' => 14],
            [['object'], 'string', 'max' => 128],

            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'title'       => Yii::t('site', 'Title'),
            'description' => Yii::t('site', 'Description'),
            'name'        => Yii::t('site', 'Name'),
            'type'        => Yii::t('site', 'Type'),
            'params'      => Yii::t('site', 'Params'),
            'attribute'   => Yii::t('site', 'Attribute'),
            'object'      => Yii::t('site', 'Object'),
            'object_id'   => Yii::t('site', 'Object ID'),
            'is_main'     => Yii::t('site', 'Main file'),
            'status'      => Yii::t('site', 'Status'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
            'created_by'  => Yii::t('site', 'Created By'),
            'updated_by'  => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }


    /** Получаем полный путь до папки с файлом в кеше
     *
     * @param null|string $prefixFile
     * @param bool|true   $absolute
     *
     * @return string
     */
    public function getPath($prefixFile = null, $absolute = true)
    {
        $folder    = 'file';
        $subFolder = $this->id;
        if ($this->object) {
            $folder = ucfirst((new ReflectionClass($this->object))->getShortName());

            if ($this->object_id) {
                $subFolder = $this->object_id . DIRECTORY_SEPARATOR . $this->id;
            }
        }

        $basePath = '';
        if ($absolute) {
            $basePath = Yii::getAlias(Yii::$app->getModule('media')->pathCache);
        }

        $path = $basePath . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $subFolder;

        if ($prefixFile) {
            $path .= DIRECTORY_SEPARATOR . $prefixFile . UrlHelper::strTourl($this->name);
        }

        return $path;
    }

    /** Получаем относительный url до файла
     * @param string $prefix
     * @param bool   $noCache
     * @param string $scheme
     * @return string
     */
    public function getUrl($prefix = 'org', $noCache = false, $scheme = '')
    {
        $path = $this->getPath($prefix, false);

        if ($this->type === self::TYPE_FILE_IMAGE) {
            /** Если это новая модель и нет изображения */
            if ($this->isNewRecord) {
                $pathNoImage = Yii::$app->getModule('media')->config['noImage']['path'];
                $fileNoImage = $prefix . Yii::$app->getModule('media')->config['noImage']['file'];

                return $pathNoImage . $fileNoImage;
            }
        }

        $originalPath = $this->getPathToOriginal();
        $absolutePath = $this->getPath($prefix);

        /*if ($this->isNewRecord) {
            //$tt =10;
        } else*/
        if (!is_file($absolutePath) && is_file($originalPath)) {
            if ($this->type === self::TYPE_FILE_IMAGE) {
                $this->saveImageSizes();
            } else {
                copy($originalPath, $absolutePath);
            }
        };

        if ($noCache) {
            $path .= '?expired=' . time();
        }

        $url = Yii::$app->getModule('media')->webBasePathFiles . $path;

        if ($scheme) {
            return rtrim(Url::to(['/'], true), '/') . $url;
        } else {
            return $url;
        }
    }


    /** Получаем полный путь до файла оригинала
     *
     * @param bool|false $withFile
     *
     * @return string
     */
    public function getPathToOriginal($withFile = true)
    {
        $basePath = Yii::getAlias(Yii::$app->getModule('media')->pathOriginal);

        $fileName = '';
        if ($withFile) {
            $fileName = $this->name = UrlHelper::strTourl($this->name);
        }

        return $basePath . DIRECTORY_SEPARATOR
            . $this->id . DIRECTORY_SEPARATOR
            . $fileName;
    }


    /** Сохраняем оригинал
     * @throws \ErrorException
     */
    private function saveOriginal()
    {
        $pathOriginal       = $this->getPathToOriginal(true);
        $pathOriginalTarget = $this->getPathToOriginal();

        if ($this->type === self::TYPE_FILE_IMAGE) {
            $module = Yii::$app->getModule('media');
            $params = $module->getConfig($this->object);
            FileHelper::saveImage(
                $pathOriginal,
                $pathOriginalTarget,
                $params[self::SIZE_ORG],
                false
            );

            FileHelper::removeDirectory($this->getPath());
        }
    }

    /**
     * Сохраняем размеры изображения
     */
    public function saveImageSizes()
    {
        $original = $this->getPathToOriginal();

        if (!is_file($original)) {
            return;
            //throw new ErrorException('Not found original image');
        }

        $imgConfig = Yii::$app->getModule('media')->getConfig($this->object);

        FileHelper::removeDirectory($this->getPath());
        FileHelper::createDirectory($this->getPath());

        foreach ($imgConfig as $prefix => $params) {
            $cachePathFile = $this->getPath($prefix);
            FileHelper::saveImage($original, $cachePathFile, $params, false);
        }
    }


    /** Сохраняем файл, если передан аргумент $url, то из интернета, если нет, то из формы
     *
     * @param string|false|UploadedFile $instance
     *
     * @return bool|null|string
     */
    public function saveFile($instance = false)
    {
        $fileSave         = null;
        $pathOriginal     = $this->getPathToOriginal(false);
        $pathOriginalFile = $this->getPathToOriginal(true);

        FileHelper::createDirectory($this->getPath());
        FileHelper::createDirectory($pathOriginal);

        if ($instance instanceof UploadedFile) {
            $file = $instance;
            if ($file) {
                $file->saveAs($pathOriginalFile);
            }
        } elseif ($instance && UrlHelper::isUrl($instance)) {
            FileHelper::saveFileFromInternet($instance, $pathOriginal, $pathOriginalFile);
        } else {
            $file = UploadedFile::getInstance($this, 'file');
            if ($file) {
                $file->saveAs($pathOriginalFile);
            }
        }
        if (is_file($pathOriginalFile)) {
            $this->saveOriginal();
            $fileSave = true;
        }
        return $fileSave;
    }

    /**
     * @param bool $insert
     *
     * @return bool|null|string
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setTypeByExtension();
            return true;
        }
        return false;
    }


    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::removeDirectory($this->getPathToOriginal());
            FileHelper::removeDirectory($this->getPath());
            return true;
        }
        return false;
    }

    /**
     * Получаем путь до изображения - no image
     *
     * @param string $size
     * @return string
     */
    public static function getNoImageUrl($size = 'ico')
    {
        $noImage = Yii::$app->getModule('media')->config['noImage'];

        return $noImage['path'] . $size . $noImage['file'];
    }


    /**
     * Устанавливаем тип файла
     */
    private function setTypeByExtension()
    {
        $this->type = self::TYPE_FILE_FILE;

        if ($this->name) {
            $ext = strtolower(substr($this->name, strrpos($this->name, '.') + 1));

            if ($ext) {
                switch ($ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                    case 'tif':
                    case 'tiff':
                        $this->type = self::TYPE_FILE_IMAGE;
                        break;
                    case 'doc':
                    case 'docx':
                        $this->type = self::TYPE_FILE_DOC;
                        break;
                    case 'xls':
                    case 'xlsx':
                        $this->type = self::TYPE_FILE_XLS;
                        break;
                    case 'zip':
                    case 'tar':
                    case 'gz':
                    case 'rar':
                        $this->type = self::TYPE_FILE_ARCHIVE;
                        break;
                    case 'mpg':
                    case 'mpeg':
                    case 'wmv':
                    case 'mp2':
                    case 'avi':
                    case 'mp4':
                        $this->type = self::TYPE_FILE_VIDEO;
                        break;
                    case 'mp3':
                    case 'mp2':
                    case 'avi':
                    case 'mp4':
                        $this->type = self::TYPE_FILE_AUDIO;
                        break;
                    case 'pdf':
                        $this->type = self::TYPE_FILE_PDF;
                        break;
                }
            }
        }
    }
}
