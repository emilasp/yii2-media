<?php

namespace emilasp\media\components\gridview;

use emilasp\media\models\File;
use Yii;
use yii\grid\Column;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * Class ImageColumn
 * @package emilasp\media\components\gridView
 */
class ImageColumn extends Column
{
    public $attribute = 'file';
    public $path      = '';
    public $options   = [
        'width'           => '25px',
    ];

    /**
     * INIT
     */
    public function init()
    {
        if (!$this->header) {
            $this->header = Yii::t('media', 'Image');
        }
        $this->grid->view->registerJs('new jBox("Image");    ', View::POS_READY);
    }

    /**
     * Формируем вывыод в колонку превью
     *
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     * @return string
     */
    public function renderDataCellContent($model, $key, $index)
    {
        $image = ArrayHelper::getValue($model, $this->attribute);

        if ($image) {
            $this->options['title'] = $this->options['alt'] = $image->title;

            $html = Html::a(
                Html::img($image->getUrl(File::SIZE_ICO), $this->options),
                $image->getUrl(File::SIZE_MAX),
                [
                    'data-jbox-image' => 'gl',
                    'data-pjax'       => 0
                ]
            );
            return $html;
        }

        return Html::img(File::getNoImageUrl(), $this->options);
    }
}
