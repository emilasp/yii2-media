<?php

namespace emilasp\media\components\stockApi;

use yii\base\Component;
use yii\httpclient\Client;


/**
 * Class ShutterstockApi
 * @package emilasp\media\components\stockApi
 */
class ShutterstockApi extends Component
{
    const ACTION_SEARCH = 'images/search';

    public $url = 'https://api.shutterstock.com/v2/';

    public $key;
    public $secret;

    /**
     * Поиск по банку
     *
     * @param string $query
     * @param array  $params
     * @return array
     */
    public function search(string $query, array $params = []): array
    {
        $params['query'] = urlencode($query);
        //$params['per_page'] = 20;

        return $this->sendRequest(self::ACTION_SEARCH, $params);
    }

    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    public function sendRequest(string $action, array $data = [], string $method = 'get', array $headers = []): ?array
    {
        $url = $this->url . $action . '?' . http_build_query($data);

        $headers['Authorization'] = 'Basic ' . base64_encode("{$this->key}:{$this->secret}");
        $headers['User-Agent']    = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0';

        $client  = new Client();
        $request = $client->createRequest()
            //->setFormat(Client::FORMAT_JSON)
            ->setMethod($method)
            ->setUrl($url)
            //->setData($data)
            ->setHeaders($headers);

        $response = $request->send();

        return $response->data;
    }
}
