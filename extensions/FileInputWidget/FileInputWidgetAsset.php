<?php
namespace emilasp\media\extensions\FileInputWidget;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class FileInputWidgetAsset
 * @package emilasp\media\extensions\FileInputWidget
 */
class FileInputWidgetAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_HEAD];

    public $sourcePath = __DIR__ . '/assets';

    public $css = ['file-input-widget.css'];
    public $js = ['file-input-widget.js'];

}
