<?php
use emilasp\media\models\File;
use yii\helpers\Html;

$file = new \emilasp\media\models\File();
if (count($files)) {
    $file = array_shift($files);
}
?>

<div class="fileinput-class" data-id="<?= $file->id ?>">

    <?= Html::hiddenInput($attribute . '[id][]', $file->id, ['class' => 'file-id']) ?>

    <div style="height: <?= $previewHeight ?>px">
        <?php if ($file->id) : ?>
            <?php if ($file->type === File::TYPE_FILE_IMAGE) : ?>
                <a href="<?= $file->getUrl('max') ?>" title="<?= $file->title ?>" data-pjax="0"
                   data-jbox-image="gallery<?= $model->id ?>">
                    <img src="<?= $file->getUrl('ico') ?>" alt="<?= $file->title ?>" class="img-thumbnail">
                </a>
            <?php else: ?>
                <?php if (File::$types[$file->type] !== File::TYPE_FILE_FILE) : ?>
                    <i class="fa fa-file-<?= File::$types[$file->type] ?>-o text-warning" aria-hidden="true"></i>
                <?php else: ?>
                    <i class="fa fa-file-o" aria-hidden="true"></i>
                <?php endif; ?>
            <?php endif; ?>


            <?php if ($showTitle) : ?>
                <a href="<?= $file->getUrl('org') ?>" target="_blank" data-pjax="0"
                    <?= $file->type === File::TYPE_FILE_IMAGE ? 'data-jbox-image="gallery' . $model->id . '"' : '' ?>>
                    <?= $file->title ?? $file->name ?>
                </a>
            <?php endif; ?>

            <?php if (!$onlyView) : ?>
                &nbsp;&nbsp;
                <a href="javascript:;" class="file-expand-upload text-info">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
            <?php endif; ?>

        <?php else: ?>
            <?php if (!$onlyView) : ?>
                <div class="file-add-separate"></div>
                <a href="javascript:;" class="file-expand-upload">
                    <i class="fa fa-plus" aria-hidden="true"></i> <?= Yii::t('media', 'Add file') ?>
                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <?php if (!$onlyView) : ?>
        <div class="file-expand-upload-block clearfix">

            <div class="float-left">
                <label class="uploadbutton">
                    <div class="button-file">Выбрать</div>
                    <div class='input'>Выберите файл</div>
                    <?= Html::fileInput($attribute . '[file][]', null, [
                        'class'    => 'file-upload-button',
                        'onchange' => 'this.previousSibling.previousSibling.innerHTML = this.value'
                    ]) ?>
                </label>
            </div>

            <?php if ($title) : ?>
                <div class="float-left">
                    <?= Html::textInput($attribute . '[title][]', $file->title, [
                        'class'       => 'form-control file-title',
                        'placeholder' => Yii::t('media', 'File title')
                    ]) ?>
                </div>
            <?php endif; ?>
            <?php if ($description) : ?>
                <div class="float-left">
                    <?= Html::textarea($attribute . '[description][]', $file->description, [
                        'class'       => 'form-control  file-description',
                        'placeholder' => Yii::t('media', 'File description'),
                        'rows'        => 1
                    ]) ?>
                </div>
            <?php endif; ?>

            <div class="float-right">
                <button type="button" class="btn btn-success upload-file" data-id="<?= $file->id ?>">
                    <?= Yii::t('media', 'Save') ?>
                </button>
            </div>
            <?php if (!$file->isNewRecord) : ?>
                <div class="float-right">
                    <button type="button" class="btn btn-danger file-delete" data-id="<?= $file->id ?>">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>
