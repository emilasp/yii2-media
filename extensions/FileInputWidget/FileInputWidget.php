<?php

namespace emilasp\media\extensions\FileInputWidget;

use yii;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * Расширение для вывода хлебных крошек
 *
 * Class FileInputWidget
 * @package emilasp\media\extensions\FileInputWidget
 */
class FileInputWidget extends yii\base\Widget
{
    const TYPE_SINGLE = 'single';
    const TYPE_MULTI  = 'multi';

    public $type = self::TYPE_SINGLE;

    public $title       = true;
    public $description = true;
    public $isMain      = true;

    /** @var bool without update/upload file */
    public $onlyView = false;


    public $id        = 'file-input';
    public $model;
    public $attribute = 'files';

    public $fileInputSelector  = '.fileinput-class';

    public $previewHeight = 25;
    public $showTitle     = true;

    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run()
    {
        Pjax::begin(['id' => 'pjax-' . $this->id]);
        echo $this->render('file-input-' . $this->type, [
            'model'         => $this->model,
            'files'         => $this->getFiles(),
            'attribute'     => $this->attribute,
            'title'         => $this->title,
            'description'   => $this->description,
            'isMain'        => $this->isMain,
            'previewHeight' => $this->previewHeight,
            'showTitle'     => $this->showTitle,
            'onlyView'      => $this->onlyView,
        ]);
        Pjax::end();
    }

    /** Получаем все файлы модели
     * @return mixed
     */
    private function getFiles()
    {
        return $this->model->{$this->attribute};
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        FileInputWidgetAsset::register($view);

        $urlToUploadFiles = Url::toRoute(['/media/file/save']);
        $urlToDeleteFile  = Url::toRoute(['/media/file/delete']);

        $model      = $this->model;
        $modelClass = urlencode($model::className());

        $id = 'pjax-' . $this->id;

        $js = <<<JS
        new jBox('Image');
        
        function getForm(file_id) {
            var formData = new FormData();
            file_id = typeof file_id !== 'undefined' ? file_id : '';
           
            jQuery.each($('{$this->fileInputSelector} input[type=file]'), function(i, files) {
                var data = $(files);
                var container = data.closest('{$this->fileInputSelector}');
                var id = container.find('.file-id').val();
                var title = container.find('.file-title').val();
                var description = container.find('.file-description').val();
                var isMain = container.find('.file-is_main').is(':checked') ? 1 : 0;
                
                
                if (id == file_id) {
                    id = typeof id !== 'undefined' ? id : '';
                    title = typeof title !== 'undefined' ? title : '';
                    description = typeof description !== 'undefined' ? description : '';
                                   
                    formData.append('id[]', id);
                    formData.append('object[]', '{$modelClass}');
                    formData.append('object_id[]', '{$this->model->id}');
                    
                    if (title) {
                       formData.append('title[]', title);
                    }
                    if (description) {
                       formData.append('description[]', description);
                    }
                    if (isMain) {
                       formData.append('is_main[]', isMain);
                    }
                    
                    jQuery.each(files.files, function(i, file) {
                        formData.append('files[]', file);
                    });     
                }                    
            });
             
            return formData;            
        }
        
        function send(formData) {
            $("#{$id}").loading(true);
            $.ajax({
                url: "{$urlToUploadFiles}",
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,			
                data: formData,
                success: function(data){
                    if(!data.errors.length){
                        notice('Файл загружен', 'success');
                        $.pjax.reload({
                            container:"#pjax-{$this->id}",
                            "timeout" : 0,
                            push:false
                        });
                    }else{
                        notice('Не удалось загрузить файл..', 'danger');				
                    }
                }
            });
        }
        
        $('body').on("pjax:end", "#pjax-{$this->id}", function() {
             new jBox('Image');
        });
        
    $('body').on('click', '.upload-file', function () {
        var file_id = $(this).data('id');
        var form = getForm(file_id);
        send(form);
    });
        
    $('body').on('click', '.file-delete', function () {
        if (confirm('Delete file?')) {
            var container = $(this).closest('.fileinput-class');
            var file_id = container.data('id');
            $.ajax({
                url: "{$urlToDeleteFile}",
                type: "POST",
                dataType : "json", 
                data: {"id": file_id},
                success: function(data){
                    if(!data.errors.length){
                        notice('Файл загружен');
                        container.remove();
                    }else{
                        notice('Не удалось загрузить файл..', 'danger');				
                    }
                }
            });
        }
    });
   
    $('body').on('click', '.file-expand-upload', function () {
        var container = $(this).closest('.fileinput-class');
        container.find('.file-expand-upload-block').slideToggle();
    });
JS;
        $this->view->registerJs($js, yii\web\View::POS_READY);
    }
}
