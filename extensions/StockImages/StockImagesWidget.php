<?php
namespace emilasp\media\extensions\StockImages;

use yii;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * Расширение для перелинковки контента
 *
 * Class ContentLinkingWidget
 * @package emilasp\media\extensions\StockImages
 */
class StockImagesWidget extends yii\base\Widget
{
    private $images = [];

    /**
     * INIT
     */
    public function init():void
    {
        parent::init();

        $this->setImages();
    }

    /**
     * RUN
     */
    public function run():void
    {
        echo $this->render('stock-images', [
            'images' => $this->images
        ]);
    }

    /**
     * Set images by request query
     */
    private function setImages():void
    {
        if (Yii::$app->request->isPjax && $query = Yii::$app->request->get('stockQuery')) {

            $images = Yii::$app->stockApi->search($query);

            foreach($images['data'] as $image) {
                $image['url'] = $image['assets']['small_thumb']['url'];
                $this->images[] = $image;
            }
        }
    }
}
