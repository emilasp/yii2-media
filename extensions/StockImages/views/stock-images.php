<?php

use yii\helpers\Url;
use yii\widgets\Pjax;

?>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalStock">STOCK изображения</button>


<div class="modal fade" id="modalStock" tabindex="-1" role="dialog" aria-labelledby="modalStock" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Изображения(STOCK)</h4>
            </div>
            <div class="modal-body">
                <?php Pjax::begin(['id' => 'stock-images']); ?>
                    <?php foreach ($images as $image): ?>
                        <img src="<?= $image['url'] ?>" width="100px" />
                    <?php endforeach; ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="modal-footer">
                <input type="text" class="form-control" id="stock-input-search" />
                <button type="button" class="btn btn-primary btn-stock-search">Search</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
$js = <<<JS
    $(document).on('click', '.btn-stock-search', function () {
        var query = $('#stock-input-search').val();
        $.pjax({
            container:"#stock-images",
            timeout: 0,
            push:false,
            data:{stockQuery:query},
            scrollTo:false
        });
    })
JS;

$this->registerJs($js);