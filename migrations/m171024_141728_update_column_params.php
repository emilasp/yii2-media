<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class m171024_141728_update_column_params*/
class m171024_141728_update_column_params extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        $this->db->createCommand('ALTER TABLE "media_file" ALTER COLUMN "params" TYPE jsonb  USING (params::jsonb);')
            ->execute();
        $this->db->createCommand('ALTER TABLE "media_file" ALTER COLUMN "params" SET DEFAULT \'{}\';')->execute();

        $this->afterMigrate();
    }

    /**
    * DOWN
    */
    public function down()
    {

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
