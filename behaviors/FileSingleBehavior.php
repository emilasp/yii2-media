<?php

namespace emilasp\media\behaviors;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\helpers\FileHelper;
use emilasp\core\helpers\UrlHelper;
use emilasp\media\models\File;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;

/**
 * Поведение добавляет модели
 *
 * Добавляем поведение модели:
 * ```php
 * 'files' => [
 *     'class'     => FileSingleBehavior::className(),
 * ],
 * ```
 *
 * Добавляем relation:
 * ```php
 * public function getImage()
 * {
 *     return $this->hasOne(File::className(), ['id' => 'image_id'])
 *         ->where(['object' => self::className(), 'object_id' => $this->id]);
 * }
 * ```
 *
 * Добавляем виджет на форму:
 * ```php
 * $form->field($model, 'imageUpload')->fileInput()
 * ```
 *
 * Class FileSingleBehavior
 * @package emilasp\media\behaviors
 */
class FileSingleBehavior extends Behavior
{
    /** @var  \yii\db\ActiveRecord */
    public $owner;

    public $attribute     = 'image_id';
    public $formAttribute = 'imageUpload';

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveSingleImage',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveSingleImage',
        ];
    }

    /**
     * Сохраняем новое изображение и присваиваем id модели
     */
    public function saveSingleImage()
    {
        $imageUpload = UploadedFile::getInstance($this->owner, $this->formAttribute);

        if (!$imageUpload && UrlHelper::isUrl($this->owner->{$this->formAttribute})) {
            $imageUpload = $this->owner->{$this->formAttribute};
        }

        if ($imageUpload) {
            $imageId = $this->addImage($imageUpload);

            $model                     = $this->owner;
            $model->{$this->attribute} = $imageId;
            $this->detach();
            $model->save();
        }
    }

    /**
     * Закрепляем новое изображение
     *
     * @param UploadedFile|string $instance
     * @param null         $name
     * @param null         $title
     * @param null         $description
     * @return mixed
     */
    public function addImage($instance = null, $name = null, $title = null, $description = null)
    {
        $model = $this->owner;

        $extension = ($instance instanceof UploadedFile) ? $this->getExtension($instance) : pathinfo($instance, PATHINFO_EXTENSION);

        $file              = new File();
        $file->title       = $title;
        $file->description = $description;
        $file->name        = ($name ?? (string)time() . '.' . $extension);
        $file->object      = $model::className();
        $file->object_id   = $model->id;
        $file->type        = File::TYPE_FILE_FILE;
        $file->status      = File::STATUS_ENABLED;
        $file->save(false);
        $file->saveFile($instance);

        return $file->id;
    }

    /**
     * Проверяем и отдаём расширение файла
     *
     * @param UploadedFile $instance
     * @return string
     */
    private function getExtension(UploadedFile $instance): string
    {
        $checkArray = ['php'];
        $ext        = $instance->getExtension();
        if (in_array($ext, $checkArray)) {
            return 'txt';
        }
        return $ext;
    }

    /**
     * @return ActiveQuery
     */
    public function getFile()
    {
        return $this->owner->hasOne(File::className(), ['id' => 'image_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getMainImage()
    {
        return $this->owner->hasOne(File::className(), ['id' => 'image_id']);
    }
}
