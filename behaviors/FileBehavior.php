<?php

namespace emilasp\media\behaviors;

use emilasp\core\helpers\UrlHelper;
use emilasp\media\models\File;
use emilasp\core\behaviors\relations\RelationBehavior;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;

/**
 * Поведение добавляет модели
 *
 * Добавляем поведение модели:
 * ```php
 * 'files' => [
 *     'class'     => FileBehavior::className(),
 *     'attribute' => 'files',
 * ],
 * ```
 *
 * Добавляем relation:
 * ```php
 * public function getFiles()
 * {
 *     return $this->hasMany(File::className(), ['object_id' => 'id'])->where(['object' => self::className()]);
 * }
 * ```
 *
 * Добавляем виджет на форму:
 * ```php
 * FileInputWidget::widget([
 *     'model'         => $model,
 *     'type'          => FileInputWidget::TYPE_MULTI,
 *     'title'         => true,
 *     'description'   => true,
 *     'showTitle'     => true,
 *     'previewHeight' => 20,
 * ])
 * ```
 *
 * Class FileBehavior
 * @package emilasp\media\behaviors
 */
class FileBehavior extends Behavior
{
    const RELATION_NAME = 'files';

    /** @var int тип файла отдаваемый по умолчанию */
    public $defaultType;

    public $attribute = self::RELATION_NAME;


    /**
     * INIT
     */
    public function init()
    {
        //$this->relationClass = File::className();
        //$this->relationName  = self::RELATION_NAME;
        //$this->attribute     = $this->relationName;
        parent::init();
    }

    /**
     * Закрепляем новое изображение
     *
     * @param UploadedFile $instance
     * @param null         $name
     * @param null         $title
     * @param null         $description
     * @return mixed
     */
    public function addImage($instance = null, $name = null, $title = null, $description = null)
    {
        $model = $this->owner;

        $file              = new File();
        $file->title       = $title;
        $file->description = $description;
        $file->name        = ($name ?? (string)time() . '.' . $this->getExtension($instance));
        $file->object      = $model::className();
        $file->object_id   = $model->id;
        $file->type        = File::TYPE_FILE_FILE;
        $file->status      = File::STATUS_ENABLED;
        $file->save(false);
        $file->saveFile($instance);

        return $file->id;
    }

    /**
     * Закрепляем изображение
     *
     * @param              $id
     * @param UploadedFile $instance
     * @param null         $name
     * @param null         $title
     * @param null         $description
     * @param null         $isMain
     * @return File|static
     */
    public function saveImage(
        $id,
        $instance = null,
        $name = null,
        $title = null,
        $description = null,
        $isMain = 0
    ): File {
        $model = $this->owner;

        if (!$id || !$file = File::findOne($id)) {
            $file = new File();

            $file->object      = $model::className();
            $file->object_id   = $model->id;
            $file->status      = File::STATUS_ENABLED;
            $file->type        = File::TYPE_FILE_IMAGE;

            if ($instance) {
                $file->name        = UrlHelper::strTourl($name ?? $instance->name ?? $file->name ?? (string)time());

                if ($name || (empty($instance->name) && empty($file->name))) {
                    $file->name        .= '.' . $this->getExtension($instance);
                }
            }
        }

        $file->title       = $title;
        $file->description = $description;
        $file->is_main     = $isMain;

        if ($isMain) {
            File::updateAll(['is_main' => 0], ['object' => $file->object, 'object_id' => $file->object_id]);
        }

        $file->save(false);

        if ($instance) {
            $file->saveFile($instance);
        }

        return $file;
    }

    /**
     * Проверяем и отдаём расширение файла
     *
     * @param UploadedFile $instance
     * @return string
     */
    private function getExtension(UploadedFile $instance): string
    {
        $checkArray = ['php'];
        $ext        = $instance->getExtension();
        if (in_array($ext, $checkArray)) {
            return 'txt';
        }
        return $ext;
    }


    /**
     * @return ActiveQuery
     */
    /*public function getFile()
    {
        return $this->owner->hasOne(File::className(), ['id' => 'image_id']);
    }*/

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->owner->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFiles()
    {
        $model = $this->owner;
        return $this->owner->hasMany(File::className(), ['object_id' => 'id'])
            ->where(['object' => $model::className()])
            ->orderBy('media_file.type DESC, media_file.id DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments()
    {
        $model = $this->owner;
        return $this->owner->hasMany(File::className(), ['object_id' => 'id'])
            ->where([
                'object' => $model::className(),
                'type' => [File::TYPE_FILE_DOC, File::TYPE_FILE_XLS, File::TYPE_FILE_PDF, File::TYPE_FILE_FILE]
            ])
            ->orderBy('media_file.id DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getImages()
    {
        $model = $this->owner;
        return $this->owner->hasMany(File::className(), ['object_id' => 'id'])
            ->where(['object' => $model::className(), 'type' => File::TYPE_FILE_IMAGE])
            ->orderBy('media_file.id DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getMainImage()
    {
        $model = $this->owner;
        $className = str_replace(['Search', '\search'], '', $model::className());
        return $this->owner->hasOne(File::className(), ['object_id' => 'id'])
            ->where(['object' => $className, 'type' => File::TYPE_FILE_IMAGE])
            ->orderBy('is_main DESC, id ASC');
    }
}
