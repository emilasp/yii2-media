<?php

namespace emilasp\media\controllers;


use emilasp\media\models\File;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use emilasp\core\components\base\Controller;
use yii\web\UploadedFile;

/**
 * File Controller
 */
class FileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'            => [
                'class' => AccessControl::className(),
                'only'  => ['save'],
                'rules' => [
                    [
                        'actions' => ['save'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'             => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'save' => ['post'],
                ],
            ],
            'contentNegotiator' => [
                'class'       => \yii\filters\ContentNegotiator::className(),
                'only'        => ['save'],
                'formatParam' => '_format',
                'formats'     => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml'  => \yii\web\Response::FORMAT_XML,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'cke-upload' => [
                'class' => 'emilasp\core\extensions\CkeEditor\actions\UploadAction',
            ],
        ];
    }

    /**
     * Save File
     *
     * @return mixed
     */
    public function actionSave()
    {
        $post  = Yii::$app->request->post();
        $files = UploadedFile::getInstancesByName('files');

        foreach ($post['object'] as $index => $object) {
            $file        = $files[$index] ?? null;
            $fileId      = $post['id'][$index] ?? null;
            $object      = urldecode($object);
            $objectId    = (int)$post['object_id'][$index];
            $title       = $post['title'][$index] ?? null;
            $description = $post['description'][$index] ?? null;
            $isMain      = $post['is_main'][$index] ?? null;

            if ($file || $fileId) {
                $model = $object::find()->where(['id' => $objectId])->drafted()->one();

                if ($model) {
                    $model->saveImage($fileId, $file, null, $title, $description, $isMain);
                }
            }


            return ['errors' => []];
        }
    }

    /**
     * Save File
     *
     * @return mixed
     */
    public function actionDelete()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        if ($file = File::findOne($id)) {
            $file->delete();
        } else {
            return ['errors' => ['No delete file!']];
        }

        return ['errors' => []];
    }
}
